+++
date = "2018-06-15T14:30:00"
title = "About me"
+++

<center>![Meade](https://thedarkula.gitlab.io/code-artistry/img/about.jpg)</center>

I'm called Meade.

I'm an omnivore. Meaning, I’m greedy in what I play with.

I find writing code fascinating. And I love Rust. I wrote a tool called "BrewStillery" originally in Ruby with a graphical interface using GTK3. Now, I've totally rewritten everything in Rust and GTK3 with an added CSS theme completely written from scratch that is beautifully unique. I've released "BrewStillery" under an Open Source license because I use Open Source software whenever possible.

I love Open Source. With that, comes having to trudge through sometimes non-existent documentation. I write down everything I do in a clear and explanatory manner. Having code publicly reviewable for security and safety is of vital importance to me.

I'm passionate about security and privacy. I've caused codebase changes to the FreeSWITCH project in regards to end to end encryption and audiophile codecs.

I like things to load as fast as physically possible.

I'm always analysing my systems because I know I can find a way to do something better, more secure and efficient. For instance, I switched my production email servers from Spamassassin to rspamd because rspamd scans so many more emails faster with less processor and ram usage, while also being more accurate.

I'm keen to learn anything and everything that excites me, whether it be new technologies, spoken languages, or a better way to brew beer.
---
title: "Unwrap Me Nicely"
date: 2018-07-16T00:33:55+01:00
draft: false
categories: ["rust"]
image: "img/UnwrapMeNicely.jpg"
---
We're going to get into implementations and functions that take `self`.
A function that takes `self` as an argument changes the order of a
function call from `functionName(argumentVariable)` to `argumentVariable.functionName()`.
This is a huge benefit because we can make our function calls read more
like a sentence.

The example above is pretty easy to read, but let's say we need to do
three function calls on one variable:

```
ouncesToGrams(poundsUSToOunces(poundsGBToUS(weightInGBPounds)));
```
Now this is just ridiculous.

We have to start at the center of
the nesting dolls and read outwards. If we change this to an
implementation that takes `self`, we can make our calls look like this:

```
weightInGBPounds.poundsGBToUS().poundsUSToOunces().ouncesToGrams()
```

This reads clear as day. We start with a GB pound, then change that to
US pounds, then to ounces, then to grams.

Here's how our program changes to use an implementation.
Also, our function changes to `self` as the argument:

```
#![allow(non_snake_case, non_camel_case_types)]

enum unwrapMe {
    hasFloat(f64),
}

impl unwrapMe {
    fn stripOut(self) -> f64 {
        match self {
            unwrapMe::hasFloat(number) => number,
        }
    }
}

fn main() {
    let nestedNumber: unwrapMe = unwrapMe::hasFloat(3.33);

    println!("{}", nestedNumber.stripOut() * 3.33);
}
```

Which outputs:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 5.90s
     Running `target/debug/postsScratchPad`
11.0889
```


`self` takes the place of whatever we implement our function(s) for.
In this case, it is our `enum` `unwrapMe`.


We can also add a `new()` function to our implementation:

```
#![allow(non_snake_case, non_camel_case_types)]

enum unwrapMe {
    hasFloat(f64),
}

impl unwrapMe {
    fn stripOut(self) -> f64 {
        match self {
            unwrapMe::hasFloat(number) => number,
        }
    }

    fn new(number: f64) -> Self {
        unwrapMe::hasFloat(number)
    }
}

fn main() {
    let nestedNumber: unwrapMe = unwrapMe::hasFloat(3.33);

    println!("{}", nestedNumber.stripOut() * 3.33);

    println!("{}", unwrapMe::new(3.33).stripOut());
}
```

This outputs:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 0.62s
     Running `target/debug/postsScratchPad`
11.0889
3.33
```

Our `new()` function takes one argument, an `f64`. It also returns
`Self`, which is different than `self`. `Self` here refers to whatever
we have an `impl` for, in this case `unwrapMe`.
It's a shortcut way of saying, "go back up to the top of the `impl`
and return whatever it is".

Essentially, use `self` as an argument, `Self` as a return type.

The way we call a non-`self` function in an `impl` block is with
the `::` syntax. So we say `unwrapMe::new(3.33)`. The great part is
that now we can rewrite our program using our `impl` block instead of
creating the `nestedNumber` variable:

```
#![allow(non_snake_case, non_camel_case_types)]

enum unwrapMe {
    hasFloat(f64),
}

impl unwrapMe {
    fn stripOut(self) -> f64 {
        match self {
            unwrapMe::hasFloat(number) => number,
        }
    }

    fn new(number: f64) -> Self {
        unwrapMe::hasFloat(number)
    }
}

fn main() {
    println!("{}", unwrapMe::new(3.33).stripOut() * 3.33);
}
```

Which outputs:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 0.57s
     Running `target/debug/postsScratchPad`
11.0889
```

Here, we've done everything in one line. We handled our enum creation,
extracting the nested value, and performing maths all in one clean,
readable line.

Rule of thumb, always avoid doing things like this:

<center>
<video width="600" controls>
  <source src="https://thedarkula.gitlab.io/code-artistry/img/MatryoshkaDolls.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
</center>

---
title: "Have A Seat"
date: 2018-09-29T14:43:03+01:00
draft: false
categories: ["rust"]
image: "img/HaveASeat.jpg"
---


Previously on Code Artistry, we learned how to parallelise our functions.

Now let's see how we can measure the performance increase.



While version:

```
pub fn increaseABVMaths(allInputs: &increaseABVData) -> finalSugarFloat {
    let mut newStartingBrix: f64 = allInputs.startingBrix;

    let mut newEstimatedABV: f64 = realABVAndAttenuation(newStartingBrix, FINAL_BRIX_IDEAL).0;

    while newEstimatedABV < allInputs.desiredABV {
        newStartingBrix = newStartingBrix + 0.001;
        newEstimatedABV = realABVAndAttenuation(newStartingBrix, FINAL_BRIX_IDEAL).0;
    }
    ...
```


Parallelised version:
```
pub fn increaseABVMaths(allInputs: &increaseABVData) -> finalSugarFloat {
    let mut newStartingBrix: f64 = allInputs.startingBrix;

    newStartingBrix = (((newStartingBrix * 1000.0) as u32)..33000)
        .into_par_iter()
        .map(|mapBrix| {
            let tempBrix: f64 = ( mapBrix as f64 / 1000.0 ) + 0.0001;
            let tempABV: f64 = realABVAndAttenuation(tempBrix,FINAL_BRIX_IDEAL).0;

            (tempBrix, tempABV)
        })
        .find_first(|(_tempBrix, tempABV)| {
            allInputs.desiredABV < *tempABV
        })
        .expect("increaseABVPrep(), newEstimatedABV")
        .0;
    ...
```

For benchmarking purposes, I changed the range on the parallel version to be like this:
`newStartingBrix = (((newStartingBrix * 1000.0) as u32)..4294967000)`
The end range is the largest number a `u32` can hold.
That way it would take quite a bit more maths to finish.

To do the benchmark, I wrote the following in the same module/file:
```
#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench(bencher: &mut Bencher) {
        let allInputs: increaseABVData = increaseABVData {
            startingBrix: 12.0,
            desiredABV: 4294967.0,
            currentVolume: 45.0,
            increaseABVUnitsEnum: imperialOrMetric::Metric,
        };

        bencher.iter(|| increaseABVMaths(&allInputs));
    }
}
```

And then to `main.rs`, I added:
```
#![feature(test)]
extern crate test;
```
to the top of the file.


`cargo bench` currently requires rust nightly to run. The best thing to do is something
like the following:

`rustup run nightly cargo bench > while`

and

`rustup run nightly cargo bench > rayon`

And comment/uncomment each respective version between running each command.

Then run `cargo install cargo-benchcmp`.

Following that, you can do `cargo benchcmp rayon while`

Which gave us the following result:

```
 name                                  rayon ns/iter  while ns/iter  diff ns/iter   diff %  speedup
 functions::increaseABV::tests::bench  6,254,142      2,788,014        -3,466,128  -55.42%   x 2.24
```

 This shows that our parallelised version is 2.24 times faster than the plain while loop.



 For `reconstructedTransmissionData()` we got this:

```
 name                                 rayon ns/iter  while ns/iter  diff ns/iter   diff %  speedup
 light::lightFunctions::tests::bench  17,530         10,450               -7,080  -40.39%   x 1.68
```

For `twoArraySum()`, we got this:

```
 name                                 rayon ns/iter  while ns/iter  diff ns/iter   diff %   speedup
 light::lightFunctions::tests::bench  12,455         104                 -12,351  -99.16%  x 119.76
```

 For `threeArraySum()`, we got this:

```
 name                                 rayon ns/iter  while ns/iter  diff ns/iter   diff %   speedup
 light::lightFunctions::tests::bench  13,832         78                  -13,754  -99.44%  x 177.33
```

For `computedIlluminantData()`, we got this:

```
 name                                                 rayon ns/iter  while ns/iter  diff ns/iter   diff %   speedup
 light::colour::computedIlluminantData::tests::bench  14,170         86                  -14,084  -99.39%  x 164.77
```


Now you can sit and chill, because we not only know we improved our
functions, but we can really see how much :)
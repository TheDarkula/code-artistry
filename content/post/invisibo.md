---
title: "Invisibo"
date: 2018-06-30T01:17:13+01:00
draft: false
categories: ["rust"]
image: "img/Invisibo.png"
---
Enums, unlike structs, effectively don't exist. They don't (have to) have any data at all.
They don't even have any meaning except for the name of the enum and the names of its variants.
So why the crap would we want to use them? Let's talk about a three way switch.


```
#![allow(non_snake_case, non_camel_case_types)]

 fn takesStr(stringy: &str) {
     match stringy {
         "imperialGB" => println!("The state of the switch is imperialGB"),
         "imperialUS" => println!("The state of the switch is imperialUS"),
         "metric" => println!("The state of the switch is metric"),
         _ => println!("We had a typo"),
     }
 }

 fn main() {
     let borrowed: &str = "imperial GB";

     takesStr(borrowed);
 }
 ```

When matching on string literals, we have to match on `_`, which is effectively like an
`else {}` block in an `if/else` statement.

Let's see what happens when we run it.

```
/usr/bin/cargo run --color=always
    Blocking waiting for file lock on build directory
   Compiling postsScratchPad v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 2.07s
     Running `target/debug/postsScratchPad`
We had a typo
```

So, why did our match statement not work? Everything looks correct.

Oh, crap. We did our `let` statement for the variable `borrowed`
with the value as `"imperial GB"`, not `"imperialGB"`.
The fact that we had a space means that none of our explicit match
arms will actually get used, screwing our program over.


What if we didn't do a `println!()`, but instead had our program
perform some maths? Now it will just fail silently. Our program
will still compile, because strings aren't verified by the compiler.
They are only evaluated at runtime.


This seems like an awful lot of responsibility on us to make
sure that every character is exactly how it should be.
There has to be a way for us to be able to make mistakes and have
the compiler check things for us instead.

### Enums to the rescuuue!


```
#![allow(non_snake_case, non_camel_case_types)]

enum imperialOrMetric {
    imperialGB,
    imperialUS,
    metric,
}

fn printsSwitchState(switch: imperialOrMetric) {
    match switch {
        imperialOrMetric::imperialGB => println!("The state of the switch is imperialGB"),
        imperialOrMetric::imperialUS => println!("The state of the switch is imperialUS"),
        imperialOrMetric::metric => println!("The state of the switch is metric"),
    }
}

fn main() {
    let switchy: imperialOrMetric = imperialOrMetric::imperialGB;

    printsSwitchState(switchy);
}
```

Whereas struct's have fields, enum's have variants. We create an enum
with a relevant name and corresponding logically named variants.
The way we create an instance of an enum is with `enumName::enumVariant`.

After running this program, we get:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
warning: variant is never constructed: `imperialUS`
 --> src/main.rs:5:5
  |
5 |     imperialUS,
  |     ^^^^^^^^^^
  |
  = note: #[warn(dead_code)] on by default

warning: variant is never constructed: `metric`
 --> src/main.rs:6:5
  |
6 |     metric,
  |     ^^^^^^

    Finished dev [unoptimized + debuginfo] target(s) in 0.67s
     Running `target/debug/postsScratchPad`
The state of the switch is imperialGB
```

First, Rust gives us a warning that we created variants that we didn't use, but
it's only because this is an incomplete example.
Then we see the output we expected. :)

The gorgeous thing about enums is that because they are verified by
the compiler, if we make a mistake, `rustc/cargo` will not build
our program. For example:

```
#![allow(non_snake_case, non_camel_case_types)]

enum imperialOrMetric {
    imperialGB,
    imperialUS,
    metric,
}

fn printsSwitchState(switch: imperialOrMetric) {
    match switch {
        imperialOrMetric::imperialGB => println!("The state of the switch is imperialGB"),
        imperialOrMetric::imperialUS => println!("The state of the switch is imperialUS"),
        imperialOrMetric::metric => println!("The state of the switch is metric"),
    }
}

fn main() {
    let switchy: imperialOrMetric = imperialOrMetric::imperialgB;

    printsSwitchState(switchy);
}
```

This yields an error:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
error[E0599]: no variant named `imperialgB` found for type `imperialOrMetric` in the current scope
  --> src/main.rs:18:37
   |
3  | enum imperialOrMetric {
   | --------------------- variant `imperialgB` not found here
...
18 |     let switchy: imperialOrMetric = imperialOrMetric::imperialgB;
   |                                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ variant not found in `imperialOrMetric`
   |
   = note: did you mean `variant::imperialGB`?

error: aborting due to previous error

For more information about this error, try `rustc --explain E0599`.
error: Could not compile `postsScratchPad`.

To learn more, run the command again with --verbose.
```

Not only does Rust tell us that we made a mistake, it is super helpful.
It analyses the variants of our enum, looks at similar spellings, and
suggests to us what it thinks the proper variant might be.

And in this case it is correct :)

Now we can program away and not worry about silly misspellings
because we can rely on the compiler to verify everything for us.
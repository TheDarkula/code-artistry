---
title: "Dereference What?"
date: 2018-07-18T11:04:49+01:00
draft: false
categories: ["rust"]
image: "img/DereferenceWhat.jpg"
---
Continuing on from our last post, let's say we wanted to use a
borrowed value for whatever reason (like we want our original variable
to live after the function call):

```
#![allow(non_snake_case, non_camel_case_types)]

trait mathyMaths {
    fn cubeIt(&self) -> f64;
}

impl mathyMaths for f32 {
    fn cubeIt(&self) -> f64 {
        (self as f64).powi(3)
    }
}

impl mathyMaths for f64 {
    fn cubeIt(&self) -> f64 {
        self.powi(3)
    }
}

fn main() {
    let cubeMe: f32 = 3.33;

    println!("{}", cubeMe.cubeIt());
}
```

When we implement `mathyMaths` for `f32`, we get yelled at:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
error[E0606]: casting `&f32` as `f64` is invalid
 --> src/main.rs:9:9
  |
9 |         (self as f64).powi(3)
  |         ^^^^^^^^^^^^^ cannot cast `&f32` as `f64`
  |
help: did you mean `*self`?
 --> src/main.rs:9:10
  |
9 |         (self as f64).powi(3)
  |          ^^^^

error: aborting due to previous error

For more information about this error, try `rustc --explain E0606`.
error: Could not compile `postsScratchPad`.

To learn more, run the command again with --verbose.
```

As we see, we `cannot cast '&f32' as 'f64'`. Rust is telling us
that in order to cast one
[primitive type](https://doc.rust-lang.org/book/second-edition/ch03-02-data-types.html)
as another, it must be an owned value. We now can learn about a special
operator in Rust: the asterisk `*`.
We use the asterisk, `*`, to *dereference* a value.

Think of it like this:
the `*` operator is equal to `-&`. So our `&self` in
`fn cubeIt(&self) -> f64;` becomes just `self`, an owned value.

We can modify our program to look like this:

```
#![allow(non_snake_case, non_camel_case_types)]

trait mathyMaths {
    fn cubeIt(&self) -> f64;
}

impl mathyMaths for f32 {
    fn cubeIt(&self) -> f64 {
        (*self as f64).powi(3)
    }
}

impl mathyMaths for f64 {
    fn cubeIt(&self) -> f64 {
        self.powi(3)
    }
}

fn main() {
    let cubeMe: f32 = 3.33;

    println!("{}", cubeMe.cubeIt());
}
```

Which gives us:

```
/usr/bin/cargo run --color=always
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/postsScratchPad`
36.92603446195227
```

If Rust ever complains about being given a reference, slap a `*` on it
and tell it:

<center>
<video width="600" controls>
  <source src="https://thedarkula.gitlab.io/code-artistry/img/DereferenceWhat.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
</center>
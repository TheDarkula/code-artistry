---
title: "ALSA Normalisation"
date: 2018-06-15T15:00:00
draft: false
categories: ["linux", "pro-audio"]
image: "img/Alsamixer.png"
---

Everyone knows that adverts are shit.

#### HEY IT'S AN ADVERT!!! DID YOU KNOW THAT I WANT YOU TO...

Nothing quite like going to the kitchen, throwing the kettle on, and then bleeding
from the ears because some psychopath has now taken over your TV and is using it to torture you
by blaring the volume at full clip.

You get the point.

Fortunately, we're programmers, and we can take control of this.

You'll need the `swh-plugins` package installed.

Edit or create /etc/asound.conf with the following contents:


```
pcm.ladcomp_compressor {
    type ladspa
    slave.pcm "ladcomp_limiter";
    plugins [{
        label dysonCompress
        input {
            #peak limit, release time, fast ratio, ratio
            controls [0 1 0.5 0.99]
        }
    }]
    hint.description "Ladcomp Compressor"
}

pcm.ladcomp_limiter {
    type ladspa
	slave.pcm "plug:ladspa_float";
    plugins [{
        label fastLookaheadLimiter
        input {
            #InputGain(Db) -20 -> +20 ; Limit (db) -20 -> 0 ; Release time (s) 0.01 -> 2
            controls [ 15 0 0.8  ]
        }
    }]
}

pcm.ladspa_float {
    type lfloat
    slave {
        pcm "dmixed"
        format "S16_LE"
    }
}

pcm.dmixed {
	type dmix;
	ipc_key 1024;
		slave {
		pcm "hw:0,0"
		channels 2;
		period_time 0
		rate 48000
		period_size 1024
		buffer_size 8192
		period_time 2048
		}
    hint.description "Dmix device (48000hz)"
}

pcm.!default {
    type plug
    slave.pcm "ladcomp_compressor"
}
```

Then just run `sudo alsactl kill rescan` to reload /etc/asound.conf

This is a great thing for TV shows and movies that have ultra loud and then super quiet parts,
but it is not advisable for music, because it crushes the dynamic range.
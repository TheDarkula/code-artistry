---
title: "New Years Update"
date: 2019-01-04T06:33:37
draft: false
categories: ["diary"]
image: "img/NewYearsUpdate2019.jpg"
---
Just a quick update.

I compiled all my Rust posts into a book and I've published it
here [here](https://thedarkula.gitlab.io/code-artistry/book/).
I also have it available
on [Smashwords](https://www.smashwords.com/books/view/915857) and
[Medium](https://medium.com/datadriveninvestor/a-practical-guide-to-rust-volume-i-995dda35ae37).

Over the last two months, I've been working heavily developing the infrastructure
for [Chainetix](https://chainetix.com) using [Habitat](https://habitat.sh).

While I was working on our codebase, I realised that a good deal of the [core-plans](https://github.com/habitat-sh/core-plans)
repository was lacking and out of date. I have been putting a ton of love into
that in my personal time.

I also found a [bug/lacking feature](https://github.com/habitat-sh/habitat/issues/6011) in the `hab`
binary. I'll be hacking on that in Rust in the very near future (when everyone gets back
from the holidays).

I'll be back to writing more Rust posts, in addition to talking more about Habitat soon.

Have a great new year! :)
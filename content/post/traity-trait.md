---
title: "Traity Trait"
date: 2018-07-17T01:46:30+01:00
draft: false
categories: ["rust"]
image: "img/TraityTrait.jpg"
---

In our last post, we implemented methods (functions in an `impl` block)
for our custom enum.

What happens if we want to implement something for a type we didn't create?

This is where traits come into play.

We're going to start with a program like this:

```
#![allow(non_snake_case, non_camel_case_types)]

fn cubeIt(inputNumber: f64) -> f64 {
    inputNumber.powi(3)
}

fn main() {
    let cubeMe: f64 = 3.33;

    println!("{}", cubeIt(cubeMe));
}
```

This outputs:

```
/usr/bin/cargo run --color=always
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/postsScratchPad`
36.926037
```

Let's just do what we did previously and change `cubeIt()` into a `self`
method:

```
#![allow(non_snake_case, non_camel_case_types)]

impl f64 {
    fn cubeIt(self) -> f64 {
        self.powi(3)
    }
}

fn main() {
    let cubeMe: f64 = 3.33;

    println!("{}", cubeMe.cubeIt());
}
```

When we run this, we get:


```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
error[E0390]: only a single inherent implementation marked with `#[lang = "f64"]` is allowed for the `f64` primitive
 --> src/main.rs:3:1
  |
3 | / impl f64 {
4 | |     fn cubeIt(self) -> f64 {
5 | |         self.powi(3)
6 | |     }
7 | | }
  | |_^
  |
help: consider using a trait to implement these methods
 --> src/main.rs:3:1
  |
3 | / impl f64 {
4 | |     fn cubeIt(self) -> f64 {
5 | |         self.powi(3)
6 | |     }
7 | | }
  | |_^

error: aborting due to previous error

For more information about this error, try `rustc --explain E0390`.
error: Could not compile `postsScratchPad`.

To learn more, run the command again with --verbose.
```

So, we've tried to just straight implement something for an `f64`, which
isn't a type that we created. It comes from the [standard library]
(https://doc.rust-lang.org/std/primitive.f64.html).

`rustc` is telling us that we need to create a trait. A trait is a block
just like an `impl`, but it only contains a method's
signature; no body.

Like this:

```
trait mathyMaths {
    fn cubeIt(&self) -> f64;
}
```

We created a trait called `mathyMaths` (name it whatever you want, just
keep it as logical as you can for what it does). The method `cubeIt()`
inside the `trait` block is stripped down. It just tells `rustc` what
to expect when we do implement it *for a type*.

```
#![allow(non_snake_case, non_camel_case_types)]

trait mathyMaths {
    fn cubeIt(self) -> f64;
}

impl mathyMaths for f64 {
    fn cubeIt(self) -> f64 {
        self.powi(3)
    }
}

fn main() {
    let cubeMe: f64 = 3.33;

    println!("{}", cubeMe.cubeIt());
}
```

This yields:

```
/usr/bin/cargo run --color=always
    Finished dev [unoptimized + debuginfo] target(s) in 0.00s
     Running `target/debug/postsScratchPad`
36.926037
```

We've implemented our trait *for `f64`*. The cool thing is, we can
implement it for other types as well:

```
#![allow(non_snake_case, non_camel_case_types)]

trait mathyMaths {
    fn cubeIt(self) -> f64;
}

impl mathyMaths for i64 {
    fn cubeIt(self) -> f64 {
        (self as f64).powi(3)
    }
}

impl mathyMaths for f32 {
    fn cubeIt(self) -> f64 {
        (self as f64).powi(3)
    }
}

impl mathyMaths for f64 {
    fn cubeIt(self) -> f64 {
        self.powi(3)
    }
}

fn main() {
    let cubeMei64: i64 = 3;

    println!("{}", cubeMei64.cubeIt());

    let cubeMef32: f32 = 3.33;

    println!("{}", cubeMef32.cubeIt());

    let cubeMef64: f64 = 3.33;

    println!("{}", cubeMef64.cubeIt());
}
```

This gives us:

```
/usr/bin/cargo run --color=always
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/postsScratchPad`
27
36.92603446195227
36.926037
```

You'll notice we've introduced something new here: `as`.
This is what is called type casting (insert acting reference here :) ).

In Rust, we can cast one type as another type. So in our `i64` and `f32`
implementations, we change the input to be an `f64`, because
like dissolves like: we can only do maths on the same type. Since
we're outputting an `f64`, we need to convert all types to that.

Remember, you can type cast in programming, but don't do it to actors.
They're real people.
---
title: "Unwrap Me"
date: 2018-07-08T12:57:19+01:00
draft: false
categories: ["rust"]
image: "img/UnwrapMe.jpg"
---

Now that we've established precisely what enums are(n't), we'll look at
another feature that enums have: nested types.

We can actually put anything we want inside an enum:

```
enum unwrapMe {
    hasFloat(f64),
}
```

Now we'll create a function to do things with it:

```
#![allow(non_snake_case, non_camel_case_types)]

enum unwrapMe {
    hasFloat(f64),
}

fn main() {
    let nestedNumber: unwrapMe = unwrapMe::hasFloat(3.33);

    println!("{}", nestedNumber * 3.33);
}
```

That didn't work as intended:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
error[E0369]: binary operation `*` cannot be applied to type `unwrapMe`
  --> src/main.rs:10:20
   |
10 |     println!("{}", nestedNumber * 3.33);
   |                    ^^^^^^^^^^^^^^^^^^^
   |
   = note: an implementation of `std::ops::Mul` might be missing for `unwrapMe`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0369`.
error: Could not compile `postsScratchPad`.

To learn more, run the command again with --verbose.
```

As per usual, we have to be completely explicit for the compiler.
We've told it to multiply an instance of our enum with an `f64`, not
the `f64` inside of it.
To be able to get to the nested value, we need to use another one
of Rust's features: the `match` statement.


```
#![allow(non_snake_case, non_camel_case_types)]

enum unwrapMe {
    hasFloat(f64),
}

fn stripOut(enumInstance: unwrapMe) -> f64 {
    match enumInstance {
        unwrapMe::hasFloat(number) => number,
    }
}

fn main() {
    let nestedNumber: unwrapMe = unwrapMe::hasFloat(3.33);

    println!("{}", stripOut(nestedNumber) * 3.33);
}
```

This gives us what we wanted:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 1.96s
     Running `target/debug/postsScratchPad`
11.0889
```

What we've done, is created a function that is the equivalent of
`.unwrap()`. We can use `.unwrap()` to accomplish the same thing as our
`stripOut()` function, but only for standard types. Since we created
our type `unwrapMe`, we have to create something from scratch.

If we search the [Rust standard library](https://doc.rust-lang.org/std/?search=unwrap)
for `unwrap`, and pick one of the `unwrap` methods for an [enum](https://doc.rust-lang.org/src/core/option.rs.html#332-337)
we'll see some very similar code. As it turns out, all of the `.unwrap()` code is
basically the same as what we've written in `stripOut()`.

We `match`ed on the whole enum declaration. The syntax by itself looks
like:

```
enum::variant(nestedType) => nestedType,
```

This is referred
to as a match arm. A `match` statement is like a really powerful `if`
statement. It allows us to get the nested type of our `enum` out so
we can do fun things with it. In our program above, we just returned it.
But, we could have done something like:

```
unwrapMe::hasFloat(number) => { println!("The number is: {}, number) },
```

We could have called any function there.

Next time we'll talk about how to fancy our function up. :)
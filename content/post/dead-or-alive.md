---
title: "Dead Or Alive"
date: 2018-06-24T15:09:33+01:00
draft: false
categories: ["rust"]
image: "img/DeadOrAlive.jpg"
---
Stemming from our [previous post](https://thedarkula.gitlab.io/code-artistry/post/easter-egg/),
we can complicate things for ourselves a bit further :)

Now that we've got two types in our struct, let's add a third
and see where that takes us.

```
#![allow(non_snake_case, non_camel_case_types)]


struct easterEgg {
    stringy: &str,
    ownedString: String,
    floaty: f64,
}


fn cubeFloaty(stringsAndFloat: &mut easterEgg) {
    stringsAndFloat.floaty = stringsAndFloat.floaty.powi(3);
}


fn main() {
    let mut stringsAndFloat: easterEgg = easterEgg {
        stringy: "The number ",
        ownedString: String::from("cubed is: "),
        floaty: 3.33,
    };

    cubeFloaty(&mut stringsAndFloat);


    println!("{}{}{}", stringsAndFloat.stringy, stringsAndFloat.ownedString, stringsAndFloat.floaty);
}
```

Running this, we get:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
error[E0106]: missing lifetime specifier
 --> src/main.rs:5:14
  |
5 |     stringy: &str,
  |              ^ expected lifetime parameter

error: aborting due to previous error
```

Now we've gone made the compiler unhappy :(

It's telling us that something odd is going on because of that borrowed
(`&`) value. The `String` type from before didn't cause us any problems
because we owned it.

The thing about Rust is that it does a whole mess of crap for us without us knowing it.
For example, if we create a string literal/string slice like this: `let borrowedString = "borrowed";`,
we didn't specify the type like we should, and we didn't specify a lifetime.
What's going on behind the scenes is this: `let borrowedString: &'static str = "borrowed";`.

In Rust, everything has a lifetime. There is one special lifetime called `'static` which
means that it lives as long as the program does.

Being that our struct field `stringy` is a borrowed value, Rust doesn't
know how long that data should live.
To fix our error, we give that field a custom lifetime like so:

```
#![allow(non_snake_case, non_camel_case_types)]


struct easterEgg {
    stringy: &'a str,
    ownedString: String,
    floaty: f64,
}

fn cubeFloaty(stringsAndFloat: &mut easterEgg) {
    stringsAndFloat.floaty = stringsAndFloat.floaty.powi(3);
}


fn main() {
    let mut stringsAndFloat: easterEgg = easterEgg {
        stringy: "The number ",
        ownedString: String::from("cubed is: "),
        floaty: 3.33,
    };

    cubeFloaty(&mut stringsAndFloat);


    println!("{}{}{}", stringsAndFloat.stringy, stringsAndFloat.ownedString, stringsAndFloat.floaty);
}
```

Which then yields:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
error[E0261]: use of undeclared lifetime name `'a`
 --> src/main.rs:5:15
  |
5 |     stringy: &'a str,
  |               ^^ undeclared lifetime

error: aborting due to previous error

For more information about this error, try `rustc --explain E0261`.
error: Could not compile `postsScratchPad`.
```

Great. Rust complains a lot. First it said we didn't give it a lifetime.
Now it's unhappy because we didn't tell it what `'a` was. So let's fix it.

```
#![allow(non_snake_case, non_camel_case_types)]


struct easterEgg<'a> {
    stringy: &'a str,
    ownedString: String,
    floaty: f64,
}

fn cubeFloaty(stringsAndFloat: &mut easterEgg) {
    stringsAndFloat.floaty = stringsAndFloat.floaty.powi(3);
}


fn main() {
    let mut stringsAndFloat: easterEgg = easterEgg {
        stringy: "The number ",
        ownedString: String::from("cubed is: "),
        floaty: 3.33,
    };

    cubeFloaty(&mut stringsAndFloat);


    println!("{}{}{}", stringsAndFloat.stringy, stringsAndFloat.ownedString, stringsAndFloat.floaty);
}
```

This finally gives us:

```
/usr/bin/cargo run --color=always
   Compiling postsScratchPad v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 0.61 secs
     Running `target/debug/postsScratchPad`
The number cubed is: 36.926037
```


So what have we done here?

Rust doesn't know whether the borrowed value should live as
long as `main() {}` or as `cubeFloatly() {}`, so, we tell it.

With the lifetime declaration (`<'a>`) when we create our struct, we say
that wherever we create an instance of `easterEgg` with `let`,
the borrowed value of `stringy` must live as long as that scope does.
In this case, the scope is `main() {}`.


We set one lifetime at the beginning of the struct (we can create
as many lifetimes as we want). The syntax for that is `<'lifetimeName>`.
Typically, everyone just uses `'a`, because it's short.
Then we specify that lifetime for the `&str` that we want to apply it to
like: `&'a str`.

Dealing with lifetimes shows how much Rust lets us take for granted.

Just remember when dealing with borrowed values that we have to be
explicit whether it is dead or alive :)

<center>
<video width="600" controls>
  <source src="https://thedarkula.gitlab.io/code-artistry/img/DeadOrAlive.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
</center>
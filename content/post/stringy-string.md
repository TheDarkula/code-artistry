---
title: "Stringy String"
date: 2018-06-16T05:49:41
draft: false
categories: ["rust"]
image: "img/StringyStringBooks.jpg"
---

One thing that a lot of people seem to struggle with when learning Rust is
the concept of ownership. It's a very human, non-mathematical concept.

If I own something, you can borrow it, but you can't change it.

Think about a library versus a bookshop. If you purchase a book,
you can write in the margins, tear out pages, do whatever you want to it.
If you borrow a book from a library, you aren't allowed to do anything but read it.

Rust uses the same concept for data.

Take a look here:

```
#![allow(non_snake_case, non_camel_case_types)]

// Say you're a function called borrowedFromLibrary(). I'm main().
// I lend you a book and tell you to set it on fire.
// You take a string literal (&str).
// You cannot destroy it because of the ampersand &, which tells us that it is a borrowed value.
// You can use what's written in the book, but you can't destroy it.

fn borrowedFromLibrary(stringy: &str) {
    drop(stringy);
}

fn main() {
    let literal: &str = "does nothing";
    borrowedFromLibrary(literal);

    // Notice here that borrowedFromLibrary() does nothing and we can still print out the variable "literal".
    // This is because Drop "will not release any borrows".
    // https://doc.rust-lang.org/std/mem/fn.drop.html

    println!("{}", literal);
}
```

When compiled, we see:

```
/usr/bin/cargo run --color=always
   Compiling stringyString v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 0.52 secs
     Running `target/debug/stringyString`
does nothing
```

So, the library made sure we followed the rules and didn't do anything nasty to their book.


Now we'll look at a bookshop example:

```
#![allow(non_snake_case, non_camel_case_types)]

// Say you're a function that's called purchasedFromBookshop(). I'm still main().
// I give you a book and tell you to set it on fire.
// You take a String.
// You are allowed to destroy it because string is an owned value.
// You can do whatever you want to the book.

fn purchasedFromBookshop(stringy: String) {
    drop(stringy);
}

fn main() {
    // Here, since the String type is owned, purchasedFromBookshop() does precisely
    // what we tell it to do, and frees the value.
    // When we try to print it out, we can't because it's gone from memory (we destroyed it).

    let actualString: String = String::from("owned");
    purchasedFromBookshop(actualString);

    println!("{}", actualString);
}
```


Running this one we see:

```
/usr/bin/cargo run --color=always
   Compiling stringyString v0.1.0
error[E0382]: use of moved value: `actualString`
  --> src/main.rs:28:20
   |
27 |     purchasedFromBookshop(actualString);
   |                           ------------ value moved here
28 |     println!("{}", actualString);
   |                    ^^^^^^^^^^^^ value used here after move
   |
   = note: move occurs because `actualString` has type `std::string::String`, which does not
   implement the `Copy` trait

error: aborting due to previous error

For more information about this error, try `rustc --explain E0382`.
error: Could not compile `stringyString`.

To learn more, run the command again with --verbose.
```


Yay! We broke it!

Now we can be total jackasses to the book because we own it.
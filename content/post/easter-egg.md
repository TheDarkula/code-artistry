---
title: "Easter Egg"
date: 2018-06-21T17:01:13+01:00
draft: false
categories: ["rust"]
image: "img/EasterEgg.jpg"
---

As in everything, rule of threes. If you have a function that takes
more than three arguments, we need a container to put it in. Think of
a bunch of Cadbury Mini Eggs.

<center>![Minis](https://thedarkula.gitlab.io/code-artistry/img/CadburyMiniEggs.jpg)</center>

Hand me those one at a time.

Exhausted yet?

Now put them inside an Easter Egg and just hand me that.
Much more efficient. All I have to do is open the one big egg to get
what's inside.


In Rust, structs are our Easter Eggs. We use them as a container for types.
We can put any type we want in them. Structs only have type declarations, no data.
This is because we're telling the compiler what to expect so it knows
how to allocate memory for how big or small our data will be.

Just as the Easter Egg has a bunch of minis in it, a struct has
individual entries called fields. Here, we're going to take
our struct and fill it with a String and an f64:

```
#![allow(non_snake_case, non_camel_case_types)]


struct easterEgg {
    ownedString: String,
    floaty: f64,
}


fn cubeFloaty(stringsAndFloat: &mut easterEgg) {
    stringsAndFloat.floaty = stringsAndFloat.floaty.powi(3);
}


fn main() {
    let mut stringsAndFloat: easterEgg = easterEgg {
        ownedString: String::from("The number cubed is: "),
        floaty: 3.33,
    };

    cubeFloaty(&mut stringsAndFloat);


    println!("{}{}", stringsAndFloat.ownedString, stringsAndFloat.floaty);
}
```

Notice that we first create the struct outside of any function.

Later, inside a function, we `let` a variable which is an instance
of our struct. We see this in our `stringsAndFloat` variable.
This is where we fill in actual data.

As demonstrated in our `println!`, we access the fields of a struct
with dot notation: `variableName.structField`.

Running this shows us:

```
/usr/bin/cargo run --color=always
    Finished dev [unoptimized + debuginfo] target(s) in 0.0 secs
     Running `target/debug/postsScratchPad`
The number cubed is: 36.926037
```



In case you don't know, `.powi()` is a function which raises a number to
an integer power. So above, we're doing `3.33^3`.
There is also a `.powf()` function that allows
us to raise a floating point number to a floating point number, so
we could do `3.33^3.33`.

Something else to note is that our `stringsAndFloat` variable is mutatable.
We need to have it this way so we can change the value of its fields
like we do when we pass it to `cubeFloaty()`. We redefine the field
`floaty` by cubing it.

As we've seen, structs make it beautifully simple to pass around a
bunch of types with just one variable/type.

Now go ahead and have some actual Cadbury Mini Eggs. You know you're drooling.